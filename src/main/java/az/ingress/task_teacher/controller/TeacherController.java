package az.ingress.task_teacher.controller;

import az.ingress.task_teacher.entity.Teacher;
import az.ingress.task_teacher.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api1")
@RequiredArgsConstructor
public class TeacherController {
    public final TeacherService teacherService;

    @GetMapping("/get/{id}")
    public Teacher getTeacherById(@PathVariable int id) {
        return teacherService.getTeacherById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTeacher(@PathVariable  int id) {
        teacherService.deleteTeacher(id);
        return "deleted id : " + id;
    }

    @PostMapping
    public String saveTeacher(@RequestBody Teacher teacher){
        teacherService.saveTeacher(teacher);
        return "saved :" + teacher.getName();
    }

    @PutMapping("/update/{id}")
    public String updateTeacher(@RequestBody  Teacher teacher,@PathVariable  int id){
        teacherService.updateTeacher(teacher, id);
        return "mssss";
    }

}

