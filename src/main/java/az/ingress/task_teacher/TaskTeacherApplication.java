package az.ingress.task_teacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskTeacherApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskTeacherApplication.class, args);
    }

}
