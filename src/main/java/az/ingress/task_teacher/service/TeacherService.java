package az.ingress.task_teacher.service;

import az.ingress.task_teacher.entity.Teacher;

public interface TeacherService {
    Teacher getTeacherById(int id);
    String saveTeacher(Teacher teacher);
    void deleteTeacher(int id);
    void updateTeacher(Teacher teacher, int id);
}
