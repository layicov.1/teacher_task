package az.ingress.task_teacher.service.Impl;

import az.ingress.task_teacher.entity.Teacher;
import az.ingress.task_teacher.repository.TeacherRepository;
import az.ingress.task_teacher.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    public final TeacherRepository teacherRepository;

    @Override
    public Teacher getTeacherById(@PathVariable  int id) { //read
        return teacherRepository.findById(id).get();
    }

    @Override
    public String saveTeacher(Teacher teacher) {  //create
        teacherRepository.save(teacher);
        return "My teacher has been saved:" + teacher.getName();
    }

    @Override
    public void deleteTeacher(@PathVariable  int id) { //delete
        teacherRepository.deleteById(id);
    }

    @Override
    public void updateTeacher(Teacher teacher,@PathVariable int id) {
        Optional<Teacher> teacher1 = teacherRepository.findById(id);
        Teacher teacher2 = teacher1.get();
        teacher2.setName(teacher.getName());
        teacher2.setSurname(teacher.getSurname());
        teacher2.setAge(teacher.getAge());
        teacher2.setLesson(teacher.getLesson());
        teacherRepository.save(teacher2);
    }
}
